package com.mao.bluemote;

import java.io.IOException;
import java.io.OutputStream;

import android.app.Activity;
import android.bluetooth.BluetoothSocket;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;
import android.widget.CompoundButton;

public class TouchManager implements View.OnTouchListener, CompoundButton.OnCheckedChangeListener{
	private BluetoothSocket mSocket;
	private Activity mActivity;
	private float x,y;
	private boolean dragging = false;
	private static final int MOVE = 0;
	private static final int DRAG = 1;
	private static final int PRESS = 2;
	private static final int RELEASE = 3;
	private static final int BUTTON1 = 1;
	private static final int BUTTON2 = 2;
	private static final int BUTTON3 = 3;
	
	public TouchManager(BluetoothSocket socket, Activity activity){
		mSocket = socket;
		mActivity = activity;
	}
	
	private void writeToSocket(int op, float x, float y){
		byte[] opbytes = BytesUtil.intToByteArray(op);
		byte[] xbytes = BytesUtil.floatToByteArray(x);
		byte[] ybytes = BytesUtil.floatToByteArray(y);
		byte[] combined = new byte[12];
		for(int i=0; i<4; i++)
			combined[i] = opbytes[i];
		for(int i=0; i<4; i++)
			combined[i+4] = xbytes[i];
		for(int i=0; i<4; i++)
			combined[i+8] = ybytes[i];
		try {
			OutputStream out = mSocket.getOutputStream();
			out.write(combined);
		} catch (IOException e) { 
			e.printStackTrace();
			mActivity.finish();
		}
	}
	
	private void writeToSocket(int op, int button){
		byte[] opbytes = BytesUtil.intToByteArray(op);
		byte[] buttonbytes = BytesUtil.intToByteArray(button);
		byte[] combined = new byte[12];
		for(int i=0; i<4; i++)
			combined[i] = opbytes[i];
		for(int i=0; i<4; i++)
			combined[i+4] = buttonbytes[i];
		for(int i=0; i<4; i++)
			combined[i+8] = 0;
		try {
			OutputStream out = mSocket.getOutputStream();
			out.write(combined);
		} catch (IOException e) { 
			e.printStackTrace();
			mActivity.finish();
		}
	}

	@Override
	public boolean onTouch(View view, MotionEvent event) {
		if(view.getId()==R.id.touchscreen)
			onTouchScreen(event);
		else onButton(view, event);
		return true;
	}
	
	public void closeSocket() throws IOException{
		mSocket.close();
	}
	
	public void onTouchScreen(MotionEvent event){
		if(event.getAction()==MotionEvent.ACTION_MOVE){
			float dx = event.getX()-x;
			float dy = event.getY()-y;
			int action = (dragging)?DRAG:MOVE;
			writeToSocket(action, dx, dy);
		}
		
		x = event.getX();
		y = event.getY();
	}
	
	public void onButton(View v, MotionEvent event){
		int button, action;
		switch(v.getId()){
		case R.id.left: button = BUTTON1;
			break;
		case R.id.middle: button = BUTTON2;
			break;
		case R.id.right: button = BUTTON3;
			break;
		default: return;
		}
		switch(event.getAction()){
		case MotionEvent.ACTION_DOWN: action = PRESS;
			break;
		case MotionEvent.ACTION_UP: action = RELEASE;
			break;
		default: return;
		}
		writeToSocket(action, button);
	}

	@Override
	public void onCheckedChanged(CompoundButton button, boolean checked) {
		dragging = checked;
		if(checked){
			writeToSocket(PRESS, BUTTON1);
		} else {
			writeToSocket(RELEASE, BUTTON1);
		}
	}
}
