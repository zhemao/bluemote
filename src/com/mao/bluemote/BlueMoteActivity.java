package com.mao.bluemote;

import java.io.IOException;
import java.util.UUID;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

public class BlueMoteActivity extends Activity {
    private static final int REQUEST_ENABLE_BT = 0;
    private static final UUID MY_UUID = UUID.fromString("7c781fbc-97a1-11e0-a40c-00242c75fb51");
    private BluetoothAdapter mBluetoothAdapter;
    private TouchManager mTouchManager;
    private Button mLeft, mMiddle, mRight;
    private ToggleButton mDrag;
    boolean connected = false;
    private View tscreen;
    
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        tscreen = findViewById(R.id.touchscreen);
        mLeft = (Button)findViewById(R.id.left);
        mMiddle = (Button)findViewById(R.id.middle);
        mRight = (Button)findViewById(R.id.right);
        mDrag = (ToggleButton)findViewById(R.id.drag);
        
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if(mBluetoothAdapter == null){
        	Toast.makeText(this, "Bluetooth is not available", Toast.LENGTH_LONG).show();
            finish();
            return;
        }
        
    }
    
    protected void onStart() {
        super.onStart();

        // If BT is not on, request that it be enabled.
        // setupChat() will then be called during onActivityResult
        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
        // Otherwise, setup the chat session
        }
        
        try {
        	Toast.makeText(BlueMoteActivity.this, "Waiting for connection", 
        			Toast.LENGTH_LONG).show();
			BluetoothServerSocket mServerSocket = 
					mBluetoothAdapter.listenUsingRfcommWithServiceRecord("BlueMote", MY_UUID);
			BluetoothSocket socket = mServerSocket.accept();
			connected = true;
			mTouchManager = new TouchManager(socket, this);
			
			for(View v: new View[]{tscreen, mLeft, mMiddle, mRight}){
				v.setOnTouchListener(mTouchManager);
			}
			mDrag.setOnCheckedChangeListener(mTouchManager);
			
			Toast.makeText(this, "Connected", Toast.LENGTH_LONG).show();
			mServerSocket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
    
    protected void onDestroy(){
    	super.onDestroy();
    	if(connected){
    		try {
    			mTouchManager.closeSocket();
    		} catch (IOException e) { e.printStackTrace(); }
    	}
    	
    }
	
}