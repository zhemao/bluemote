#ifndef __SETTINGS_H__
#define __SETTINGS_H__

typedef struct {
	char addr[19];
} settings;

int read_whole(char * fname, char **buf);
settings * parse_ini();
void write_ini(settings * set);

#endif /* __SETTINGS_H__ */

