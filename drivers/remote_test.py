#!/usr/bin/env python2

import sys
import bluetooth as bt
import struct

BUFLEN = 12 

def listen(sock):
	data = sock.recv(BUFLEN)
	while data:
		if len(data) == 12:
			packet = struct.unpack('<iff',data)
			print(packet)
			data = sock.recv(BUFLEN)
		else:
			print('Length: '+str(len(data)))

if __name__ == '__main__':
	bdaddr = ''
	print('Querying devices...')
	devices = [(bt.lookup_name(bdaddr), bdaddr) for bdaddr in bt.discover_devices()]
	if len(devices) == 0:
		print('''No devices found. If you know the bluetooth address of your device, 
			please enter it now. Otherwise, enter a blank line, make your device 
			discoverable and try again''')
		bdaddr = raw_input('bdaddr> ').rstrip()
		if not bdaddr: exit()
	else:
		print('We found these devices')
		for i,pair in enumerate(devices):
			name, addr = pair
			print(str(i+1)+') '+name+'\t'+addr)
		print('Which one do you want?')
		i = int(raw_input('number> '))-1
		name, bdaddr = devices[i]
	ready = raw_input('Are you ready to connect? [y/n]: ')
	if ready=='y':
		services = bt.find_service(name='BlueMote', address=bdaddr)
		if len(services) == 0:
			print('Could not find the BlueMote service')
		else:
			port = services[0]['port']
			sock = bt.BluetoothSocket()
			sock.connect((bdaddr, port))
			listen(sock)
	
