#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "btutils.h"

int read_packet(int sock, packet * pack){
	char buf[sizeof(packet)];
	char * sub = (char *)pack;
	int res = 12;
	int len = 0;
	while( len < sizeof(packet)){
		res = read(sock, buf, sizeof(packet)-len);
		if( res <= 0 ){
			return res;
		}
		memcpy(sub+len, buf, res);
		len+=res;
	}
	pack->op = btohl(pack->op);
	pack->arg1 = btohl(pack->arg1);
	pack->arg2 = btohl(pack->arg2);
	return len;
}

int open_device(int * dev_id, int * sock){
	*dev_id = hci_get_route(NULL);
	*sock = hci_open_dev( *dev_id );
	if (*dev_id < 0 || *sock < 0) {
		return -1;
	}
	return 0;
}

int get_info(int dev_id, inquiry_info * info){
	int len, max_rsp, num_rsp, flags;
	inquiry_info *ii = NULL;
	len = 8;
	max_rsp = 255;
	flags = IREQ_CACHE_FLUSH;
	num_rsp = hci_inquiry(dev_id, len, max_rsp, NULL, &info, flags);
	return num_rsp;
}

int find_device(){
	inquiry_info *info = NULL;
	int num_rsp;
	int dev_id, sock;
	int i, res;
	char addr[19];
	char name[248];
	char confname[64];
	FILE * conffile;

	res = open_device(&dev_id, &sock);
	if(res < 0){
		perror("opening device");
		exit(1);
	}
	info = (inquiry_info*) malloc(sizeof(inquiry_info));
	num_rsp = get_info(dev_id, info);

	if(num_rsp <= 0){
		printf("Could not find any devices.\n");
		free(info);
		close(sock);
		exit(1);
	}

	printf("Discovered %d devices.\n", num_rsp);
	for (i = 0; i < num_rsp; i++) {
		memset(addr, 0, sizeof(addr));
		ba2str(&(info[i].bdaddr), addr);
		memset(name, 0, sizeof(name));
		if (hci_read_remote_name(sock, &(info[i].bdaddr), sizeof(name),name, 0) < 0)
			strcpy(name, "[unknown]");
		printf("%d. %s  %s\n", i+1, addr, name);
	}
	printf("Which device do you wish to use? ");
	scanf("%d", &i);
	i -= 1;
	memset(addr, 0, sizeof(addr));
	ba2str(&(info[i].bdaddr), addr);
	
	snprintf(confname, 64, "%s/.bluemoterc", getenv("HOME"));
	conffile = fopen(confname, "w");
	fprintf(conffile, "addr=%s", addr);
	
	free( info );
	fclose(conffile);
	close( sock );
}

uint8_t extract_channel(sdp_list_t *r ){
	// go through each of the service records
	for (; r; r = r->next ) {
		sdp_record_t *rec = (sdp_record_t*) r->data;
		sdp_list_t *proto_list;
		
		// get a list of the protocol sequences
		if( sdp_get_access_protos( rec, &proto_list ) == 0 ) {
			sdp_list_t *p = proto_list;

			// go through each protocol sequence
			for( ; p ; p = p->next ) {
				sdp_list_t *pds = (sdp_list_t*)p->data;

				// go through each protocol list of the protocol sequence
				for( ; pds ; pds = pds->next ) {

					// check the protocol attributes
					sdp_data_t *d = (sdp_data_t*)pds->data;
					int proto = 0;
					for( ; d; d = d->next ) {
						switch( d->dtd ) { 
							case SDP_UUID16:
							case SDP_UUID32:
							case SDP_UUID128:
								proto = sdp_uuid_to_proto( &d->val.uuid );
								break;
							case SDP_UINT8:
								if( proto == RFCOMM_UUID ) {
									return d->val.int8;
								}
								break;
						}
					}
				}
				sdp_list_free( (sdp_list_t*)p->data, 0 );
			}
			sdp_list_free( proto_list, 0 );

		}
		sdp_record_free( rec );
	}
	return 1;
}

int find_service(struct sockaddr_rc * addr, settings * set){
	sdp_list_t *response_list = NULL, *search_list, *attrid_list;
	sdp_session_t *session = 0;
	uuid_t svc_uuid;
	uint32_t range;
	int err;
	
	session = sdp_connect( BDADDR_ANY, &(addr->rc_bdaddr), SDP_RETRY_IF_BUSY );
	sdp_uuid128_create( &svc_uuid, UUID);
	search_list = sdp_list_append( NULL, &svc_uuid );
	
	range = 0x0000ffff;
	attrid_list = sdp_list_append( NULL, &range );
	
	err = sdp_service_search_attr_req( session, search_list, 
				SDP_ATTR_REQ_RANGE, attrid_list, &response_list);
				
	if(err < 0) {
		return -1;
	}
	
	addr->rc_channel = extract_channel(response_list);
	return 0;
}

