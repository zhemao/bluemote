#include "settings.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int read_whole(char * fname, char **buf){
	long len;
	FILE * f = fopen(fname, "r");
	fseek(f, 0, SEEK_END);
	len = ftell(f);
	fseek(f, 0, SEEK_SET);
	*buf = (char*)malloc(len+sizeof(char));
	fread(*buf, 1, len, f);
	fclose(f);
	return len;
}

settings * parse_ini(){
	char *buf,  *tok, *pivot;
	char key[32], fname[64];
	settings * set = (settings*)malloc(sizeof(settings));
	snprintf(fname, 64, "%s/.bluemoterc", getenv("HOME"));
	read_whole(fname, &buf);
	tok = strtok(buf, "\n");
	while(tok){
		pivot = strchr(tok, '=');
		memset(key, 0, 32);
		strncpy(key, tok, (size_t)(pivot-tok));
		if(strcmp(key, "addr")==0){
			memset(set->addr, 0, sizeof(set->addr));
			strncpy(set->addr, pivot+1, sizeof(set->addr)-1);
		}
		tok = strtok(NULL, "\n");
	}
	free(buf);
	return set;
}

void write_ini(settings * set){
	char fname[64];
	FILE * f;
	snprintf(fname, 64, "%s/.bluemoterc", getenv("HOME"));
	f = fopen(fname, "w");
	fprintf(f, "addr=%s", set->addr);
	fclose(f);
}
