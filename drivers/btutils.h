#ifndef __BTUTILS_H__
#define __BTUTILS_H__

#include <bluetooth/bluetooth.h>
#include <bluetooth/hci.h>
#include <bluetooth/hci_lib.h>
#include <bluetooth/sdp.h>
#include <bluetooth/sdp_lib.h>
#include <bluetooth/rfcomm.h>
#include "settings.h"

#define UUID "|x\x1f\xbc\x97\xa1\x11\xe0\xa4\x0c\x00$,u\xfbQ"

typedef struct {
	unsigned int op;
	unsigned int arg1;
	unsigned int arg2;
} packet;

typedef struct {
	int op;
	float x;
	float y;
} move_event;

typedef struct {
	int op;
	int button;
} click_event;

int read_packet(int sock, packet * pack);
int open_device(int * dev_id, int * sock);
int get_info(int dev_id, inquiry_info * info);
int find_service(struct sockaddr_rc * addr, settings * set);

#endif /* __BTUTILS_H__ */

