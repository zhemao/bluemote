#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/socket.h>
#include "btutils.h"
#include "settings.h"
#include "xutils.h"

#define MOVE 0
#define DRAG 1
#define PRESS 2
#define RELEASE 3

void setup(){
	inquiry_info *info = NULL;
	int num_rsp, dev_id, sock, i, res;
	char addr[19], name[248], confname[64];
	FILE * conffile;

	res = open_device(&dev_id, &sock);
	if(res < 0){
		perror("opening device");
		exit(1);
	}
	info = (inquiry_info*) malloc(sizeof(inquiry_info));
	num_rsp = get_info(dev_id, info);

	if(num_rsp <= 0){
		printf("Could not find any devices.\n");
		free(info);
		close(sock);
		exit(1);
	}

	printf("Discovered %d devices.\n", num_rsp);
	for (i = 0; i < num_rsp; i++) {
		memset(addr, 0, sizeof(addr));
		ba2str(&(info[i].bdaddr), addr);
		memset(name, 0, sizeof(name));
		if (hci_read_remote_name(sock, &(info[i].bdaddr), sizeof(name),name, 0) < 0)
			strcpy(name, "[unknown]");
		printf("%d. %s  %s\n", i+1, addr, name);
	}
	printf("Which device do you wish to use? ");
	scanf("%d", &i);
	i -= 1;
	memset(addr, 0, sizeof(addr));
	ba2str(&(info[i].bdaddr), addr);
	
	snprintf(confname, 64, "%s/.bluemoterc", getenv("HOME"));
	conffile = fopen(confname, "w");
	fprintf(conffile, "addr=%s", addr);
	
	free( info );
	fclose(conffile);
	close( sock );
}

void connect_and_dispatch(Display *display, settings *set){
	struct sockaddr_rc addr;
	packet pack;
	move_event mevent;
	click_event cevent;
	int sock, status;

	sock = socket(AF_BLUETOOTH, SOCK_STREAM, BTPROTO_RFCOMM);
	if(sock < 0){
		perror("socket()");
		exit(1);
	}

	addr.rc_family = AF_BLUETOOTH;
	str2ba( set->addr, &addr.rc_bdaddr );
	status = find_service(&addr, set);
	if(status < 0){
		perror("find_service()");
		return;
	}

	printf("connecting\n");

	status = connect(sock, (struct sockaddr *)&addr, sizeof(addr));
	if(status < 0){
		perror("connect()");
		return;
	}

	printf("connected\n");

	printf("reading\n");
	while((status = read_packet(sock, &pack)) > 0){
		printf("status: %d\n", status);
		if(pack.op == MOVE || pack.op == DRAG){
			memcpy(&mevent, &pack, sizeof(mevent));
			printf("%f %f\n", mevent.x, mevent.y);
			mouse_move(display, (int)mevent.x, (int)mevent.y);
		} else if(pack.op == PRESS){
			memcpy(&cevent, &pack, sizeof(cevent));
			mouse_press(display, cevent.button);
		} else if(pack.op == RELEASE){
			memcpy(&cevent, &pack, sizeof(cevent));
			mouse_release(display, cevent.button);
		}else {
			printf("%d %d %d\n", pack.op, pack.arg1, pack.arg2);
		}
	}

	close(sock);
}

void run(){
	settings * set;
	Display *display = XOpenDisplay(NULL);

	if(display == NULL){
		printf("Error: could not open X display.");
		exit(1);
	}

	set = parse_ini();

	while(1){
		connect_and_dispatch(display, set);
	}

	free(set);
}


int main(int argc, char **argv){
	if(argc < 2){
		printf("Usage: %s command [args...]\n", argv[0]);
		exit(1);
	}
	if(strcmp(argv[1],"setup")==0){
		setup();
	} else if(strcmp(argv[1], "run")==0){
		run();
	}
	return 0;
}
