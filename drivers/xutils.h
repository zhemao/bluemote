#ifndef __XUTILS_H__
#define __XUTILS_H__

#include <X11/Xlib.h>

void mouse_coords(Display *display, int *x, int *y);
void mouse_move(Display *display, int x, int y);
void mouse_press(Display *display, int button);
void mouse_release(Display *display, int button);

#endif /* __XUTILS_H__ */

