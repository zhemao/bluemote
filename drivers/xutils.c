#include "xutils.h"
#include <unistd.h>
#include <string.h>

void mouse_coords (Display *display, int *x, int *y){
	XEvent event;
	XQueryPointer (display, DefaultRootWindow (display),
			&event.xbutton.root, &event.xbutton.window,
			&event.xbutton.x_root, &event.xbutton.y_root,
			&event.xbutton.x, &event.xbutton.y,
			&event.xbutton.state);
	*x = event.xbutton.x;
	*y = event.xbutton.y;
}

void mouse_move (Display *display, int dx, int dy){
	XWarpPointer (display, None, None, 0,0,0,0, dx, dy);
	XFlush (display);
	usleep (1);
}

void setup_event(Display *display, XEvent * event, int button){
	memset(event, 0, sizeof (event));
	event->xbutton.button = button;
	event->xbutton.same_screen = True;
	event->xbutton.subwindow = DefaultRootWindow (display);
	while (event->xbutton.subwindow){
		event->xbutton.window = event->xbutton.subwindow;
		XQueryPointer (display, event->xbutton.window,
			&event->xbutton.root, &event->xbutton.subwindow,
			&event->xbutton.x_root, &event->xbutton.y_root,
			&event->xbutton.x, &event->xbutton.y,
			&event->xbutton.state);
	}
}

void mouse_press(Display *display, int button){
	XEvent event;

	setup_event(display, &event, button);
	event.type = ButtonPress;
	XSendEvent (display, PointerWindow, True, ButtonPressMask, &event);
	XFlush (display);
	usleep (1);
}

void mouse_release(Display *display, int button){
	XEvent event;

	setup_event(display, &event, button);
	event.type = ButtonRelease;
	XSendEvent (display, PointerWindow, True, ButtonPressMask, &event);
	XFlush (display);
	usleep (1);
}

