#include "settings.h"
#include <stdlib.h>
#include <stdio.h>

int main(void){
	char fname[64];
	settings * set;
	char * addr;
	snprintf(fname, 64, "%s/.bluemoterc", getenv("HOME"));
	set = parse_ini(fname);
	printf("addr=%s\n", set->addr);
	free(set);
}
